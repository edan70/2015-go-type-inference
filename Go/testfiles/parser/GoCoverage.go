var global1 int = 1;
var global2 float64 = 1.0;
var global3 bool = true;
var global4 = 2;
var global5 = 2.0;
var global6 = false;

func func1() int {
	return 1;
}

func func2() bool {
	return true;
}

func func3() float64 {
	return 1.0;
}

func func4() {

}

func func5(t1 int, t2 bool, t3 float64) {

}

func main() {
	// Single-line comments

	var a int = 1;
	var b bool = true;
	var c float64 = 1.1;
	var a2 = a;
	a3 := a2;

	a4 := 1 + a;
	a5 := 1 - a;
	a6 := 1 * a;
	a7 := 1 / a;
	a8 := 1 % a;

	const d1 int = 1;
	const d2 float64 = 1.0;
	const d3 bool = true;
	const d4 = d1;

	var e1 int;
	var e2 bool;
	var e3 float64;

	var f1 int = func1();
	var f2 bool = func2();
	var f3 float64 = func3();

	func4(f1, f2, f3);

	if 1 > 2 {

	}

	if t := 1; t > 2 {

	}

	for {

	}

	for 1 > 2 {

	}

	for t := 1; ; {

	}

	for ; ; a = a + 1 {

	}

	for t := 1; t < 10; {

	}

	for t := 1; ; t = t + 1 {

	}

	for ; a < 10; a = a + 1 {

	}

	for t := 1; t < 10; t = t + 1 {

	}
}
